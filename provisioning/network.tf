# Cloud provider
provider "aws" {
  	region     = "us-west-2"
	shared_credentials_file = "${var.creds}"
}

# The VPC
resource "aws_vpc" "builds_vpc" {

    cidr_block  = "${var.builds_vpc_cidr}"
    enable_dns_hostnames = true
	tags {
	  Name = "builds_vpc"
	}
}

# Internet Gateway
resource "aws_internet_gateway" "gw" {
    vpc_id = "${aws_vpc.builds_vpc.id}"
}

# Subnet
resource "aws_subnet" "sub_us-west-2a" {

    availability_zone       = "us-west-2a" 
    vpc_id                  = "${aws_vpc.builds_vpc.id}"
    cidr_block              = "${var.sub_us-west-2a_cidr}"
    map_public_ip_on_launch = true
    depends_on              = ["aws_internet_gateway.gw"]

}

# Subnet
resource "aws_subnet" "sub_us-west-2b" {

    availability_zone       = "us-west-2b" 
    vpc_id                  = "${aws_vpc.builds_vpc.id}"
    cidr_block              = "${var.sub_us-west-2b_cidr}"
    map_public_ip_on_launch = true
    depends_on              = ["aws_internet_gateway.gw"]

}
