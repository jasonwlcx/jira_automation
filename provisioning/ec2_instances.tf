resource "aws_key_pair" "builds" {
  key_name   = "builds"
  public_key = "${var.builds_key_pair}"
}

resource "aws_instance" "development_setup" {
  ami             	= "ami-ba602bc2"
  instance_type   	= "t2.micro"
  key_name			= "builds"
  subnet_id         = "${aws_subnet.sub_us-west-2b.id}"
  security_groups 	= ["${aws_security_group.builds_sg.id}"]
  associate_public_ip_address = "true"
  disable_api_termination     = "false"
  tags {
    Name = "Jira"
  }
  
  provisioner "remote-exec" {
      inline = ["sudo apt-get update && sudo apt -y install python-minimal"]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = "sed -i -r s/[0-9]+.[0-9]+.[0-9]+.[0-9]+/${self.public_ip}/g ~/provisioning/hosts.yml && ansible-playbook -i ~/provisioning/hosts.yml --private-key ${var.ssh_key_private} ~/provisioning/development.yml" 
  }
}
