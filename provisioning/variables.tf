# Specify the builds_key-pair as var
variable "builds_key_pair" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABA jason@"
}

# Specify the private_key for provisioning development_setup
variable "ssh_key_private" {
    default = "/path/to/privkey.pem"
} 
# Specify the shared_credentials file as var
variable "creds" {
    default = "/path/to/.aws/credentials"
}              
               
               
# Specify the VPC address range as var
variable "builds_vpc_cidr" {
    default = "170.cat.0.0/16"
}

# Specify the builds_subnet-2a address range as var
variable "sub_us-west-2a_cidr" {
    default = "171.isos.0.0/24"
}

# Specify the builds_subnet-2b address range as var
variable "sub_us-west-2b_cidr" {
    default = "17x.3.11.spiderman/24"
}

# Specify an output var for the Builds ec2-instance id
output jira_instance_id {
    value = "${aws_instance.Jira.id}"
}
