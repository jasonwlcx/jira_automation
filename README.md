# Jira Install


* **Create the development host and acquire ssh key pair for use with the new host use ssh-add with the new key.** 
* **Install applications to help facilitate the setup of the development host (terraform, aws-cli, boto3, ansible).**
* **Setup ~/.ssh/config with the stanza to connect to the development host referencing the new identity file.**
    * Use controlmaster within ~/.ssh/config to multiplex ssh connections through one tcp connection.
    * Test that you can connect to the box using the generated keypair.


* **Create the directory structure using ansible-galaxy on the local machine to house the automation scripts.** 
* **Create the role "development_setup" as our main role definition, call the role from (provisioning/development.yml).**
* **Create hosts.yml and include the new host IP as inventory to facilitate running ansible tasks on that target.**
* **Create host_vars/development as our file for variable definitions specific to our new target.**


* **Define the list of requirements necessary for installing Java.**
	* Create task definitions for java-jdk1.8 (provisioning/roles/development_setup/tasks/jdk1.8.0_191.yml). 
    * Download and Unzip OracleJava-jdk1.8 using the link provided to the Oracle website - not the openJDK for linux using apt.
    * Set the jdk1.8.0 version of java as the default using **update-alternatives**. 
    * Add the JAVA_HOME environment variable to /etc/environment.


* **Define the list of requirements necessary for installing PostgreSQL.**
	* Create task definitions for desired db (provisioning/roles/development_setup/tasks/postgresql.yml)
    * Install postgresql with dependencies i.e. psycopg2 db connector through apt.
    * Create a new postgreSQL database for jira.
    * Test that a connection can be made to the newly created database.
    

* **Define the list of requirements necessary for installing Jira.**
	* Create task definitions for Jira (provisioning/roles/development_setup/tasks/jira.yml).
    * Create jira user.
    * Create the JIRA_HOME environment variable to house application-data. 
    * Download the sample data for import into the jira db.
    * Use jinja template to define a startup script to be run on boot.


* **Define the list of imports for our development_setup role to locate all tasks.**
	* Import individual tasks into the main.yml file with import statements and facilitating tags (provisioning/roles/development_setup/tasks/main.yml).


* **Run each of the task definitions by tag to isolate the tasks to ensure they are correct.**

    * ```ansible-playbook ~/provisioning/development.yml -i ~/provisioning/hosts.yml --tags system --private-key privkey.pem```


    * ```ansible-playbook ~/provisioning/development.yml -i ~/provisioning/hosts.yml --tags jdk1.8.0_191 --private-key privkey.pem```


    * ```ansible-playbook ~/provisioning/development.yml -i ~/provisioning/hosts.yml --tags postgresql --private-key privkey.pem```


    * ```ansible-playbook ~/provisioning/development.yml -i ~/provisioning/hosts.yml --tags jira7.7.4 --private-key privkey.pem```


* **When ready run the main playbook without tags to confirm that all tasks are run successfully together.**


    * ``` ansible-playbook ~/provisioning/development.yml -i ~/provisioning/hosts.yml --private-key privkey.pem```


* **Add new ec2 resource to ec2_instances.tf with remote-exec and local-exec call to the ansible playbook.**
    * ``` terraform plan ```


* **Confirm that all tasks are run successfully together through the terraform provisioning scripts.**
    * ``` terraform apply --auto-approve```




# Room from improvement

* **Use Ansible Vault to hide secrets**
* **Specify a separate role to install postgresql onto dedicated machine**
* **Test if port 8080 is in use rather than assuming it is already in use**
* **Allow automation for OS agnosticism**
* **Automate load of demo-data through the cli or playbook**



# Gotchas

* **I attempted Jira install with java8-oracle which was not compatible with Jira - workaround by using jdk1.8.0_191 directly from Oracle website**
* **I created jira database with the en_US.UTF-8 collation - workaround by adjusting the collation to C**
* **I reinstalled Jira after loading demo-data and not specifying my current atlassian jira license - workaround by reinstalling and loading demo-data with my license**
* **After loading demo-data I had to reset admin password from psql to gain entry to the application**
* **I needed an ansible.cfg with host_key_checking: False included in the project**
